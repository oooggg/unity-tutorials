﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Movement))]
[RequireComponent(typeof(Attack))]
public class UserInput : MonoBehaviour {

	private Movement movement;
	private Attack attack;

	void Start (){
		movement = GetComponent<Movement>();
		attack = GetComponent<Attack>();
	}

	// Update is called once per frame
	void Update () {
		movement.moveDirection = new Vector2(Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"));
		if(Input.GetMouseButtonUp (0)){
			attack.DoAttack(Camera.main.ScreenToWorldPoint (Input.mousePosition));
		}
	}
}
