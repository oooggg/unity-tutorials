﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	public Vector3 moveDirection;
	[SerializeField] private float speed;

	// Update is called once per frame
	void Update () {
		transform.position += moveDirection * speed * Time.deltaTime;
	}
}
