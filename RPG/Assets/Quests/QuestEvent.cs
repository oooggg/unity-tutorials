﻿using UnityEngine;
using System.Collections;

public class QuestEvent  {
	public QuestEventType questEventType;
	public string entity;

	public QuestEvent (QuestEventType questEventType, string entity){
		this.questEventType = questEventType;
		this.entity = entity;
	}
 
}

public enum QuestEventType {Destroy, Obtain, Loose, Arrive}
